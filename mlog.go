package mlog

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path"
	"sync/atomic"
)

//LogLevel type
type LogLevel int32

const (
	//LevelTrace logs everything
	LevelTrace LogLevel = 1 << iota

	//LevelInfo logs info, Warnings and Errors
	LevelInfo

	//LevelWarn logs Warnings and Errors
	LevelWarn

	//LevelError logs just Errors
	LevelError
)

//MaxBytes 宣告紀錄檔案最大紀錄容量
const MaxBytes int = 100 * 1024 * 1024 //100MB
//BackupCount 宣告紀錄檔案個數
const BackupCount int = 10

type mlog struct {
	LogLevel int32

	Trace   *log.Logger
	Info    *log.Logger
	Warning *log.Logger
	Error   *log.Logger
	Fatal   *log.Logger

	LogFile *RotatingFileHandler
}

const (
	colorRed = uint8(iota + 91)
	colorGreen
	colorYellow
	colorBlue
	colorMagenta
)

//Logger 宣告型態為mlog
var Logger mlog

//DefaultFlags used by creating loggers
var DefaultFlags = log.Ldate | log.Ltime | log.Lshortfile

//RotatingFileHandler writes log to the file, if file size exceed maxBytes then backup current file and open a new one
//Max backup file number is set by backupCount, it will delete oldest if backup files count is greater than backupCount
type RotatingFileHandler struct {
	fd *os.File

	fileName    string
	maxBytes    int
	backupCount int
}

//NewRotatingFileHandler creates dirs and opens the logfile
func NewRotatingFileHandler(fileName string, maxBytes int, backupCount int) (*RotatingFileHandler, error) {
	if maxBytes <= 0 {
		return nil, fmt.Errorf("invalid max bytes")
	}

	dir := path.Dir(fileName)
	if err := createDirIfNotExist(dir); err != nil {
		log.Fatalln("create log file error : ", err)
	}
	//if err := os.Mkdir(dir, 0777); err != nil {
	//	log.Fatalln("create log file error : ", err)
	//}

	r := new(RotatingFileHandler)

	r.fileName = fileName
	r.maxBytes = maxBytes
	r.backupCount = backupCount

	var err error
	r.fd, err = os.OpenFile(fileName, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		return nil, err
	}

	//回傳目前處理的檔案
	return r, nil
}

//Write 寫入檔案前先檢查是否需要進行rollover
func (r *RotatingFileHandler) Write(p []byte) (n int, err error) {
	if err := r.doRollover(); err != nil {
		return 0, err
	} else {
		return r.fd.Write(p)
	}
}

//Close 關閉紀錄檔
func (r *RotatingFileHandler) Close() error {
	if r.fd != nil {
		return r.fd.Close()
	}
	return nil
}

func (r *RotatingFileHandler) doRollover() error {
	f, err := r.fd.Stat()
	if err != nil { //Stat error
		return err
	}

	if f.Size() < int64(r.maxBytes) {
		return nil //file size still less than max bytes, we don't have to rollover file
	}

	// 宣告備份檔案數 > 0
	if r.backupCount > 0 {
		r.fd.Close() //關閉目前所寫的紀錄檔
		for i := r.backupCount - 1; i > 0; i-- { //將原本的檔案往後位移一個位置，例如原本1號檔案移到2號檔案，所以最舊的檔案會被覆蓋掉
			sfn := fmt.Sprintf("%s.%d", r.fileName, i) //來源檔案
			if !fileExists(sfn) {//當來源檔案存在時才需要搬移
				continue
			}
			dfn := fmt.Sprintf("%s.%d", r.fileName, i+1) //更名檔案
			if err := os.Rename(sfn, dfn); err != nil {
				return fmt.Errorf("%#v", err)
			}
			log.Printf("rollover %#v to %#v", sfn, dfn)
		}

		//單獨處理1號檔案，將目前所寫的紀錄檔更名為1號檔案
		dfn := fmt.Sprintf("%s.1", r.fileName)
		if err := os.Rename(r.fileName, dfn); err != nil {
			return fmt.Errorf("%#v", err)
		}

		//重新開啟一個新的紀錄檔
		r.fd, err = os.OpenFile(r.fileName, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)

		return err
	}

	return nil
}

//Start 開始備份
func Start(level LogLevel, fileName string) {
	doLogging(level, fileName, MaxBytes, BackupCount)
}

//StartEx 開始備份，客製化設定值
func StartEx(level LogLevel, fileName string, maxBytes, backupCount int) {
	doLogging(level, fileName, maxBytes, backupCount)
}

//Stop 停止備份
func Stop() error {
	if Logger.LogFile != nil { //關閉RotatingFileHandler
		return Logger.LogFile.Close()
	}
	return nil
}

//Sync commits the current contents of the file to stable storage.
//Typically, this means flushing the file system's in-memory copy
//of recently written data to disk.
func Sync() {
	if Logger.LogFile != nil {
		if err := Logger.LogFile.fd.Sync(); err != nil {
			log.Println("sync error : ", err)
		}
	}
}

func doLogging(logLevel LogLevel, fileName string, maxBytes, backupCount int) {
	traceHandler := ioutil.Discard
	infoHandler := ioutil.Discard
	warnHandler := ioutil.Discard
	errorHandler := ioutil.Discard
	fatalHandler := ioutil.Discard

	var fileHandler *RotatingFileHandler

	switch logLevel {
	case LevelTrace:
		traceHandler = os.Stdout
		fallthrough
	case LevelInfo:
		infoHandler = os.Stdout
		fallthrough
	case LevelWarn:
		warnHandler = os.Stdout
		fallthrough
	case LevelError:
		errorHandler = os.Stderr
		fatalHandler = os.Stderr
	}

	if fileName != "" {
		var err error
		fileHandler, err = NewRotatingFileHandler(fileName, maxBytes, backupCount)
		if err != nil {
			log.Fatal("mlog : unable to create RotatingFileHandler: ", err)
		}

		if traceHandler == os.Stdout {
			traceHandler = io.MultiWriter(fileHandler, traceHandler)
		}

		if infoHandler == os.Stdout {
			infoHandler = io.MultiWriter(fileHandler, infoHandler)
		}

		if warnHandler == os.Stdout {
			warnHandler = io.MultiWriter(fileHandler, warnHandler)
		}

		if errorHandler == os.Stderr {
			errorHandler = io.MultiWriter(fileHandler, errorHandler)
		}

		if fatalHandler == os.Stderr {
			fatalHandler = io.MultiWriter(fileHandler, fatalHandler)
		}
	}

	Logger = mlog{
		Trace:   log.New(traceHandler, yellow("[ TRACE ]: "), DefaultFlags),
		Info:    log.New(infoHandler, green("[ INFO ]: "), DefaultFlags),
		Warning: log.New(warnHandler, magenta("[ WARN ]: "), DefaultFlags),
		Error:   log.New(errorHandler, red("[ ERROR ]: "), DefaultFlags),
		Fatal:   log.New(errorHandler, blue("[ FATAL ]: "), DefaultFlags),
		LogFile: fileHandler,
	}

	atomic.StoreInt32(&Logger.LogLevel, int32(logLevel))
}

// Trace writes to the Trace destination
func Trace(format string, a ...interface{}) {
	_ = Logger.Trace.Output(2, Blue(fmt.Sprintf(format, a...)))
}

// Info writes to the Info destination
func Info(format string, a ...interface{}) {
	_ = Logger.Info.Output(2, Green(fmt.Sprintf(format, a...)))
}

// Warning writes to the Warning destination
func Warning(format string, a ...interface{}) {
	_ = Logger.Warning.Output(2, Magenta(fmt.Sprintf(format, a...)))
}

// Error writes to the Error destination and accepts an err
func Error(format string, a ...interface{} /*err error*/) {
	_ = Logger.Error.Output(2, Yellow(fmt.Sprintf(format, a...)))
}

// IfError is a shortcut function for log.Error if error
func IfError(err error) {
	if err != nil {
		_ = Logger.Error.Output(2, Yellow(fmt.Sprintf("%s\n", err)))
	}
}

// Fatal writes to the Fatal destination and exits with an error 255 code
func Fatal(a ...interface{}) {
	_ = Logger.Fatal.Output(2, Red(fmt.Sprint(a...)))
	Sync()
	os.Exit(255)
}

// Fatalf writes to the Fatal destination and exits with an error 255 code
func Fatalf(format string, a ...interface{}) {
	_ = Logger.Fatal.Output(2, Red(fmt.Sprintf(format, a...)))
	Sync()
	os.Exit(255)
}

// FatalIfError is a shortcut function for log.Fatalf if error and
// exits with an error 255 code
func FatalIfError(err error) {
	if err != nil {
		_ = Logger.Fatal.Output(2, Red(fmt.Sprintf("%s\n", err)))
		Sync()
		os.Exit(255)
	}
}

func red(s string) string {
	return fmt.Sprintf("\x1b[%dm%s\x1b[0m", colorRed, s)
}
func green(s string) string {
	return fmt.Sprintf("\x1b[%dm%s\x1b[0m", colorGreen, s)
}
func yellow(s string) string {
	return fmt.Sprintf("\x1b[%dm%s\x1b[0m", colorYellow, s)
}
func blue(s string) string {
	return fmt.Sprintf("\x1b[%dm%s\x1b[0m", colorBlue, s)
}
func magenta(s string) string {
	return fmt.Sprintf("\x1b[%dm%s\x1b[0m", colorMagenta, s)
}

//Red 回傳顯示Red顏色字串
func Red(s string) string {
	return red(s)
}

//Green 回傳顯示Green顏色字串
func Green(s string) string {
	return green(s)
}

//Yellow 回傳顯示Yellow顏色字串
func Yellow(s string) string {
	return yellow(s)
}

//Blue 回傳顯示Blue顏色字串
func Blue(s string) string {
	return blue(s)
}

//Magenta 回傳顯示Magenta顏色字串
func Magenta(s string) string {
	return magenta(s)
}

func fileExists(filename string) bool  {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func createDirIfNotExist(dir string) error {
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		err = os.MkdirAll(dir, 0755)
		return err
	}
	return nil
}
