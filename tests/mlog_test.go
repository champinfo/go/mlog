package tests

import (
	"git.championtek.com.tw/go/mlog"
	"os"
	"path/filepath"
	"runtime"
	"testing"
)

func TestMlog(t *testing.T) {
	_, currentFilePath, _, _ := runtime.Caller(0)
	// Root folder of this project
	root := filepath.Join(filepath.Dir(currentFilePath), "..")

	mlog.StartEx(mlog.LevelTrace, root + "/test_log", 1024, 10)
	for i := 0; i < 100000; i++  {
		mlog.Trace("%d - Server pid=%d started with processes: %d\n", i, os.Getpid(), runtime.GOMAXPROCS(runtime.NumCPU()))
	}
	_ = mlog.Stop()
}